## DNSSEC key handling for .NET

Rage4.DNS.DNSSEC generates keys for DNSSEC (Secure DNS), as defined in RFC 2535 and RFC 4034

### Supported key types
- Key signing key (257)
- Zone signing key (256)

### Supported algorithms
- RSASHA1 (5)
- RSASHA1NSEC3SHA1 (7)
- RSASHA256 (8)
- RSASHA512 (10)
- ECDSAP256SHA256 (13)
- ECDSAP384SHA384 (14)
- ED25519 (15)
- ED448 (16)

### Hash types
- SHA1 (1)
- SHA256 (2)
- SHA384 (4)

## Tech

Rage4.DNS.DNSSEC uses Portable.BouncyCastle for cryptographic operations

## Usage

Create new ECDSAP256SHA256 key signing key instance
```csharp
var provider = new DNSSECKeyProvider();
var key = provider.CreateDNSSECKey(KeyAlgorithm.ECDSAP256SHA256);
key.CreateKey(KeyType.KeySigningKey);
```
Import key from string
```csharp
var provider = new DNSSECKeyProvider();
var key = provider.LoadDNSSECKey(data);
```

Get the private key for storage
```csharp
var privateKey = key.GetPrivateKey();
```

Get the DNSKEY
```csharp
var dnsKey = key.GetDNSKEY();
```

Get the KeyTag
```csharp
var keyTag = key.GetKeyTag(KeyType.KeySigningKey);
```

Get the SHA256 DS for ``example.com``
```csharp
var ds = key.GetDS("example.com.", KeyType.KeySigningKey, HashType.SHA256);
```
## License

MIT
