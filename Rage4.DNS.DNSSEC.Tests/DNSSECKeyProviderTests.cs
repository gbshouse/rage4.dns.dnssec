﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using NUnit.Framework;

namespace Rage4.DNS.DNSSEC.Tests
{
	[TestFixture]
	public class DNSSECKeyProviderTests
	{
		[Test]
		[TestCaseSource("Algo")]
		public void CreateDNSSECKey(KeyAlgorithm algo, HashType hash, KeyType type, int? bits, int expectedBits)
		{
			var provider = new DNSSECKeyProvider();
			var newkey = provider.CreateDNSSECKey(algo);
			newkey.CreateKey(type, bits);

			Assert.NotNull(newkey);
			Assert.NotNull(newkey.GetPrivateKey());
			Assert.NotNull(newkey.GetDNSKEY());
			Assert.NotNull(newkey.GetKeyTag(type));
			Assert.NotNull(newkey.GetDS("example.com.", type, hash));
			Assert.AreEqual(algo, newkey.Algorithm);
			Assert.AreEqual(expectedBits, newkey.GetBits());
		}

		[Test]
		[TestCaseSource("Algo")]
		public void LoadDNSSECKeyFromCreateKey(KeyAlgorithm algo, HashType hash, KeyType type, int? bits, int expectedBits)
		{
			var provider = new DNSSECKeyProvider();
			var newkey = provider.CreateDNSSECKey(algo);
			newkey.CreateKey(type, bits);

			Assert.NotNull(newkey);

			var loadkey = provider.LoadDNSSECKey(newkey.GetPrivateKey());

			Assert.NotNull(loadkey);
			Assert.AreEqual(newkey.GetDNSKEY(), loadkey.GetDNSKEY());
			Assert.AreEqual(newkey.GetKeyTag(type), loadkey.GetKeyTag(type));
			Assert.AreEqual(newkey.GetDS("example.com.", type, hash), loadkey.GetDS("example.com.", type, hash));
            Assert.AreEqual(algo, newkey.Algorithm);
            Assert.AreEqual(expectedBits, newkey.GetBits());
        }

		[Test]
		[TestCaseSource("Keys")]
		public void LoadDNSSECKeyFromExternal(string data, string dnskey, int keytag, string resource, string ds, int hash)
		{
			var provider = new DNSSECKeyProvider();
			var loadkey = provider.LoadDNSSECKey(data);

			Assert.NotNull(loadkey);
			Assert.AreEqual(data, loadkey.GetPrivateKey());
			Assert.AreEqual(dnskey, loadkey.GetDNSKEY());
			Assert.AreEqual(keytag, loadkey.GetKeyTag(KeyType.KeySigningKey));

			if (!String.IsNullOrEmpty(resource) && !String.IsNullOrEmpty(ds) && hash > 0)
				Assert.AreEqual(ds, loadkey.GetDS(resource, KeyType.KeySigningKey, (HashType)hash));
		}

		public static IEnumerable<TestCaseData> Algo
        {
            get
            {
                yield return new TestCaseData(KeyAlgorithm.RSASHA1, HashType.SHA1, KeyType.KeySigningKey, null, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1, HashType.SHA1, KeyType.ZoneSigningKey, null, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1, HashType.SHA1, KeyType.KeySigningKey, 1024, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1, HashType.SHA1, KeyType.ZoneSigningKey, 1024, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1, HashType.SHA1, KeyType.KeySigningKey, 2048, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1, HashType.SHA1, KeyType.ZoneSigningKey, 2048, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1, HashType.SHA1, KeyType.KeySigningKey, 4096, 4096);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1, HashType.SHA1, KeyType.ZoneSigningKey, 4096, 4096);

                yield return new TestCaseData(KeyAlgorithm.RSASHA1_NSEC3_SHA1, HashType.SHA1, KeyType.KeySigningKey, null, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1_NSEC3_SHA1, HashType.SHA1, KeyType.ZoneSigningKey, null, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1_NSEC3_SHA1, HashType.SHA1, KeyType.KeySigningKey, 1024, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1_NSEC3_SHA1, HashType.SHA1, KeyType.ZoneSigningKey, 1024, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1_NSEC3_SHA1, HashType.SHA1, KeyType.KeySigningKey, 2048, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1_NSEC3_SHA1, HashType.SHA1, KeyType.ZoneSigningKey, 2048, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1_NSEC3_SHA1, HashType.SHA1, KeyType.KeySigningKey, 4096, 4096);
                yield return new TestCaseData(KeyAlgorithm.RSASHA1_NSEC3_SHA1, HashType.SHA1, KeyType.ZoneSigningKey, 4096, 4096);

                yield return new TestCaseData(KeyAlgorithm.RSASHA256, HashType.SHA1, KeyType.KeySigningKey, null, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA256, HashType.SHA1, KeyType.ZoneSigningKey, null, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA256, HashType.SHA1, KeyType.KeySigningKey, 1024, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA256, HashType.SHA1, KeyType.ZoneSigningKey, 1024, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA256, HashType.SHA1, KeyType.KeySigningKey, 2048, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA256, HashType.SHA1, KeyType.ZoneSigningKey, 2048, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA256, HashType.SHA1, KeyType.KeySigningKey, 4096, 4096);
                yield return new TestCaseData(KeyAlgorithm.RSASHA256, HashType.SHA1, KeyType.ZoneSigningKey, 4096, 4096);

                yield return new TestCaseData(KeyAlgorithm.RSASHA512, HashType.SHA1, KeyType.KeySigningKey, null, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA512, HashType.SHA1, KeyType.ZoneSigningKey, null, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA512, HashType.SHA1, KeyType.KeySigningKey, 1024, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA512, HashType.SHA1, KeyType.ZoneSigningKey, 1024, 1024);
                yield return new TestCaseData(KeyAlgorithm.RSASHA512, HashType.SHA1, KeyType.KeySigningKey, 2048, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA512, HashType.SHA1, KeyType.ZoneSigningKey, 2048, 2048);
                yield return new TestCaseData(KeyAlgorithm.RSASHA512, HashType.SHA1, KeyType.KeySigningKey, 4096, 4096);
                yield return new TestCaseData(KeyAlgorithm.RSASHA512, HashType.SHA1, KeyType.ZoneSigningKey, 4096, 4096);

                yield return new TestCaseData(KeyAlgorithm.ECDSAP256SHA256, HashType.SHA1, KeyType.KeySigningKey, null, 256);
                yield return new TestCaseData(KeyAlgorithm.ECDSAP256SHA256, HashType.SHA1, KeyType.ZoneSigningKey, null, 256);

                yield return new TestCaseData(KeyAlgorithm.ECDSAP384SHA384, HashType.SHA1, KeyType.KeySigningKey, null, 384);
                yield return new TestCaseData(KeyAlgorithm.ECDSAP384SHA384, HashType.SHA1, KeyType.ZoneSigningKey, null, 384);

                yield return new TestCaseData(KeyAlgorithm.ED25519, HashType.SHA1, KeyType.KeySigningKey, null, 256);
                yield return new TestCaseData(KeyAlgorithm.ED25519, HashType.SHA1, KeyType.ZoneSigningKey, null, 256);

                yield return new TestCaseData(KeyAlgorithm.ED448, HashType.SHA1, KeyType.KeySigningKey, null, 456);
                yield return new TestCaseData(KeyAlgorithm.ED448, HashType.SHA1, KeyType.ZoneSigningKey, null, 456);


            }
		}

		public static IEnumerable<TestCaseData> Keys
		{
			get
			{
				yield return new TestCaseData(
					"Private-key-format: v1.2\nAlgorithm: 13 (ECDSAP256SHA256)\nPrivateKey: GU6SnQ/Ou+xC5RumuIUIuJZteXT2z0O/ok1s38Et6mQ=",
					"GojIhhXUN/u4v54ZQqGSnyhWJwaubCvTmeexv7bR6edbkrSqQpF64cYbcB7wNcP+e+MAnLr+Wi9xMWyQLc8NAA==",
					55648,
					"example.net.", "b4c8c1fe2e7477127b27115656ad6256f424625bf5c1e2770ce6d6e37df61d17", 2
					);
				yield return new TestCaseData(
					"Private-key-format: v1.2\nAlgorithm: 14 (ECDSAP384SHA384)\nPrivateKey: WURgWHCcYIYUPWgeLmiPY2DJJk02vgrmTfitxgqcL4vwW7BOrbawVmVe0d9V94SR",
					"xKYaNhWdGOfJ+nPrL8/arkwf2EY3MDJ+SErKivBVSum1w/egsXvSADtNJhyem5RCOpgQ6K8X1DRSEkrbYQ+OB+v8/uX45NBwY8rp65F6Glur8I/mlVNgF6W/qTI37m40",
					10771,
					"example.net.", "72d7b62976ce06438e9c0bf319013cf801f09ecc84b8d7e9495f27e305c6a9b0563a9b5f4d288405c3008a946df983d6", 4
					);
				yield return new TestCaseData(
					"Private-key-format: v1.2\nAlgorithm: 15 (ED25519)\nPrivateKey: ODIyNjAzODQ2MjgwODAxMjI2NDUxOTAyMDQxNDIyNjI=",
					"l02Woi0iS8Aa25FQkUd9RMzZHJpBoRQwAQEX1SxZJA4=",
					3613,
					"example.com.", "3aa5ab37efce57f737fc1627013fee07bdf241bd10f3b1964ab55c78e79a304b", 2
					);
				yield return new TestCaseData(
					"Private-key-format: v1.2\nAlgorithm: 15 (ED25519)\nPrivateKey: DSSF3o0s0f+ElWzj9E/Osxw8hLpk55chkmx0LYN5WiY=",
					"zPnZ/QwEe7S8C5SPz2OfS5RR40ATk2/rYnE9xHIEijs=",
					35217,
					"example.com.", "401781b934e392de492ec77ae2e15d70f6575a1c0bc59c5275c04ebe80c6614c", 2
					);
				yield return new TestCaseData(
					"Private-key-format: v1.2\nAlgorithm: 16 (ED448)\nPrivateKey: xZ+5Cgm463xugtkY5B0Jx6erFTXp13rYegst0qRtNsOYnaVpMx0Z/c5EiA9x8wWbDDct/U3FhYWA",
					"3kgROaDjrh0H2iuixWBrc8g2EpBBLCdGzHmn+G2MpTPhpj/OiBVHHSfPodx1FYYUcJKm1MDpJtIA",
					9713,
					"example.com.", "6ccf18d5bc5d7fc2fceb1d59d17321402f2aa8d368048db93dd811f5cb2b19c7", 2
					);
				yield return new TestCaseData(
					"Private-key-format: v1.2\nAlgorithm: 16 (ED448)\nPrivateKey: WEykD3ht3MHkU8iH4uVOLz8JLwtRBSqiBoM6fF72+Mrp/u5gjxuB1DV6NnPO2BlZdz4hdSTkOdOA",
					"kkreGWoccSDmUBGAe7+zsbG6ZAFQp+syPmYUurBRQc3tDjeMCJcVMRDmgcNLp5HlHAMy12VoISsA",
					38353,
					"example.com.", "645ff078b3568f5852b70cb60e8e696cc77b75bfaaffc118cf79cbda1ba28af4", 2
					);

				yield return new TestCaseData(
					"Private-key-format: v1.2\nAlgorithm: 8 (RSASHA256)\nModulus: wVwaxrHF2CK64aYKRUibLiH30KpPuPBjel7E8ZydQW1HYWHfoGmidzC2RnhwCC293hCzw+TFR2nqn8OVSY5t2Q==\nPublicExponent: AQAB\nPrivateExponent: UR44xX6zB3eaeyvTRzmskHADrPCmPWnr8dxsNwiDGHzrMKLN+i/HAam+97HxIKVWNDH2ba9Mf1SA8xu9dcHZAQ==\nPrime1: 4c8IvFu1AVXGWeFLLFh5vs7fbdzdC6U82fduE6KkSWk=\nPrime2: 2zZpBE8ZXVnL74QjG4zINlDfH+EOEtjJJ3RtaYDugvE=\nExponent1: G2xAPFfK0KGxGANDVNxd1K1c9wOmmJ51mGbzKFFNMFk=\nExponent2: GYxP1Pa7CAwtHm8SAGX594qZVofOMhgd6YFCNyeVpKE=\nCoefficient: icQdNRjlZGPmuJm2TIadubcO8X7V4y07aVhX464tx8Q=",
					"AwEAAcFcGsaxxdgiuuGmCkVImy4h99CqT7jwY3pexPGcnUFtR2Fh36BponcwtkZ4cAgtvd4Qs8PkxUdp6p/DlUmObdk=",
					9033 + 1,
					null, null, 0
					);
				yield return new TestCaseData(
					"Private-key-format: v1.2\nAlgorithm: 10 (RSASHA512)\nModulus: 0eg1M5b563zoq4k5ZEOnWmd2/BvpjzedJVdfIsDcMuuhE5SQ3pfQ7qmdaeMlC6Nf8DKGoUPGPXe06cP27/WRODtxXquSUytkO0kJDk8KX8PtA0+yBWwy7UnZDyCkynO00Uuk8HPVtZeMO1pHtlAGVnc8VjXZlNKdyit99waaE4s=\nPublicExponent: AQAB\nPrivateExponent: rFS1IPbJllFFgFc33B5DDlC1egO8e81P4fFadODbp56V7sphKa6AZQCx8NYAew6VXFFPAKTw41QdHnK5kIYOwxvfFDjDcUGza88qbjyrDPSJenkeZbISMUSSqy7AMFzEolkk6WSn6k3thUVRgSlqDoOV3SEIAsrB043XzGrKIVE=\nPrime1: 8mbtsu9Tl9v7tKSHdCIeprLIQXQLzxlSZun5T1n/OjvXSUtvD7xnZJ+LHqaBj1dIgMbCq2U8O04QVcK3TS9GiQ==\nPrime2: 3a6gkfs74d0Jb7yL4j4adAif4fcp7ZrGt7G5NRVDDY/Mv4TERAKMa0TKN3okKE0A7X+Rv2K84mhT4QLDlllEcw==\nExponent1: v3D5A9uuCn5rgVR7wgV8ba0/KSpsdSiLgsoA42GxiB1gvvs7gJMMmVTDu/ZG1p1ZnpLbhh/S/Qd/MSwyNlxC+Q==\nExponent2: m+ezf9dsDvYQK+gzjOLWYeKq5xWYBEYFGa3BLocMiF4oxkzOZ3JPZSWU/h1Fjp5RV7aPP0Vmx+hNjYMPIQ8Y5w==\nCoefficient: Je5YhYpUron/WdOXjxNAxDubAp3i5X7UOUfhJcyIggqwY86IE0Q/Bk0Dw4SC9zxnsimmdBXW2Izd8Lwuk8FQcQ==",
					"AwEAAdHoNTOW+et86KuJOWRDp1pndvwb6Y83nSVXXyLA3DLroROUkN6X0O6pnWnjJQujX/AyhqFDxj13tOnD9u/1kTg7cV6rklMrZDtJCQ5PCl/D7QNPsgVsMu1J2Q8gpMpztNFLpPBz1bWXjDtaR7ZQBlZ3PFY12ZTSncorffcGmhOL",
					3740 + 1,
					null, null, 0
					);
			}
		}
	}
}
