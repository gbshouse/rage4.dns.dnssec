﻿using System;
using System.Collections.Generic;

namespace Rage4.DNS.DNSSEC
{
	public class DNSSECKeyProvider : IDNSSECKeyProvider
	{
		private readonly string ALGORITHM = "Algorithm";

		public DNSSECKeyProvider() { }

		public IDNSSECKey CreateDNSSECKey(KeyAlgorithm keyAlgorithm)
		{
			return GetDNSSECKeyByAlgorithm(keyAlgorithm);
		}

		public IDNSSECKey LoadDNSSECKey(string input)
		{
			var parameters = Parse(input);

			if (!parameters.TryGetValue(ALGORITHM, out string algorithm))
				throw new Exception("Unable to parse key!");

			if (!Enum.TryParse(algorithm, out KeyAlgorithm keyAlgorithm))
				throw new Exception("Unsupported algorithm");

			return GetDNSSECKeyByAlgorithm(keyAlgorithm, parameters);
		}

		private IDNSSECKey GetDNSSECKeyByAlgorithm(KeyAlgorithm keyAlgorithm)
		{
			switch (keyAlgorithm)
			{
				case KeyAlgorithm.RSASHA1:
					return new RSASHA1DNSSECKey();
				case KeyAlgorithm.RSASHA1_NSEC3_SHA1:
					return new RSASHA1NSEC3SHA1DNSSECKey();
				case KeyAlgorithm.RSASHA256:
					return new RSASHA256DNSSECKey();
				case KeyAlgorithm.RSASHA512:
					return new RSASHA512DNSSECKey();
				case KeyAlgorithm.ECDSAP256SHA256:
					return new ECDSAP256SHA256DNSSECKey();
				case KeyAlgorithm.ECDSAP384SHA384:
					return new ECDSAP384SHA384DNSSECKey();
				case KeyAlgorithm.ED25519:
					return new ED25519DNSSECKey();
				case KeyAlgorithm.ED448:
					return new ED448DNSSECKey();
				default:
					throw new ArgumentException("Unsupported algorithm", nameof(keyAlgorithm));
			}
		}

		private IDNSSECKey GetDNSSECKeyByAlgorithm(KeyAlgorithm keyAlgorithm, Dictionary<string, string> parameters)
		{
			switch (keyAlgorithm)
			{
				case KeyAlgorithm.RSASHA1:
					return new RSASHA1DNSSECKey(parameters);
				case KeyAlgorithm.RSASHA1_NSEC3_SHA1:
					return new RSASHA1NSEC3SHA1DNSSECKey(parameters);
				case KeyAlgorithm.RSASHA256:
					return new RSASHA256DNSSECKey(parameters);
				case KeyAlgorithm.RSASHA512:
					return new RSASHA512DNSSECKey(parameters);
				case KeyAlgorithm.ECDSAP256SHA256:
					return new ECDSAP256SHA256DNSSECKey(parameters);
				case KeyAlgorithm.ECDSAP384SHA384:
					return new ECDSAP384SHA384DNSSECKey(parameters);
				case KeyAlgorithm.ED25519:
					return new ED25519DNSSECKey(parameters);
				case KeyAlgorithm.ED448:
					return new ED448DNSSECKey(parameters);
				default:
					throw new ArgumentException("Unsupported algorithm", nameof(keyAlgorithm));
			}
		}

		private Dictionary<string, string> Parse(string data)
		{
			var dict = new Dictionary<string, string>();
			string[] parts;

			foreach (string line in data.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries))
			{
				parts = line.Split(new string[] { ": " }, StringSplitOptions.None);

				if (parts.Length == 2 && !dict.ContainsKey(parts[0]))
				{
					if (parts[0].Equals(ALGORITHM))
					{
						parts = parts[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
						dict.Add(ALGORITHM, parts[0].ToString());
					}
					else
					{
						dict.Add(parts[0], parts[1].Trim());
					}
				}
			}

			return dict;
		}
	}

}
