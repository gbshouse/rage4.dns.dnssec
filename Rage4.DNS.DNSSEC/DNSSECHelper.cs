﻿using System;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Digests;

namespace Rage4.DNS.DNSSEC
{
	internal static class DNSSECHelper
	{
		public static int CreateKeyTag(KeyType type, string base64key, KeyAlgorithm algorithm)
		{
			string part1 = String.Empty.ToWireFormat((int)type, 3, (int)algorithm).ToLower();
			string part2 = BitConverter.ToString(Convert.FromBase64String(base64key.Replace(" ", ""))).Replace("-", "").ToLower();
			byte[] buffer = (part1 + part2).HexStringToByteArray();

			int foot = 0;

			int i;

			for (i = 0; i < buffer.Length - 1; i += 2)
				foot += (((buffer[i] & 0xFF) << 8) + (buffer[i + 1] & 0xFF));

			if (i < buffer.Length)
				foot += ((buffer[i] & 0xFF) << 8);

			foot += ((foot >> 16) & 0xFFFF);

			return (foot & 0xFFFF);
		}

		public static string CreateDS(string domain, KeyType type, string base64key, KeyAlgorithm algorithm, HashType hash)
		{
			if (!domain.EndsWith("."))
				domain += ".";

			string part1 = domain.ToWireFormat((int)type, 3, (int)algorithm).ToLower();
			string part2 = BitConverter.ToString(Convert.FromBase64String(base64key)).Replace("-", "").ToLower();

			return CalculateDigest((part1 + part2).HexStringToByteArray(), hash);
		}

		public static IDigest GetDigest(HashType hashType)
		{
			switch (hashType)
			{
				case HashType.SHA1:
					return new Sha1Digest();
				case HashType.SHA256:
					return new Sha256Digest();
				case HashType.SHA384:
					return new Sha384Digest();
				default:
					throw new NotImplementedException($"{hashType} is not supported");
			}
		}

		public static string CalculateDigest(byte[] value, HashType hashType)
		{
			IDigest digest = GetDigest(hashType);
			digest.BlockUpdate(value, 0, value.Length);
			byte[] hash = new byte[digest.GetDigestSize()];
			digest.DoFinal(hash, 0);
			return hash.ToHexString();
		}
	}

}
