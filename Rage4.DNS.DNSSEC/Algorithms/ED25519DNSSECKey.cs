﻿using System;
using System.Collections.Generic;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace Rage4.DNS.DNSSEC
{
	public class ED25519DNSSECKey : DNSSECKey
	{
		private Ed25519PrivateKeyParameters privateParameters;
		private Ed25519PublicKeyParameters publicParameters;

		public ED25519DNSSECKey() : base(KeyAlgorithm.ED25519) { }

		public ED25519DNSSECKey(Dictionary<string, string> parameters) : base(KeyAlgorithm.ED25519) {
			privateParameters = new Ed25519PrivateKeyParameters(Convert.FromBase64String(parameters["PrivateKey"]), 0);
			publicParameters = privateParameters.GeneratePublicKey();
		}

		public override void CreateKey(KeyType keyType, int? bits)
		{
			var generator = new Ed25519KeyPairGenerator();
			generator.Init(new Ed25519KeyGenerationParameters(new SecureRandom()));

			var keyPair = generator.GenerateKeyPair();
			privateParameters = (Ed25519PrivateKeyParameters)keyPair.Private;
			publicParameters = (Ed25519PublicKeyParameters)keyPair.Public;
		}

		public override string GetPrivateKey()
		{
			return String.Format("Private-key-format: v1.2\nAlgorithm: {0} ({1})\nPrivateKey: {2}",
				(int)Algorithm, Enum.GetName(typeof(KeyAlgorithm), Algorithm),
				Convert.ToBase64String(privateParameters.GetEncoded())).Replace("_", "-");
		}

		public override byte[] GetPublicBytes()
		{
			return publicParameters.GetEncoded();
		}

		public override int GetBits() => 256;
    }
}
