﻿using System.Collections.Generic;
using Org.BouncyCastle.Asn1.Sec;

namespace Rage4.DNS.DNSSEC
{
	public class ECDSAP384SHA384DNSSECKey : ECDSADNSSECKey
	{
		public ECDSAP384SHA384DNSSECKey() : base(SecObjectIdentifiers.SecP384r1, KeyAlgorithm.ECDSAP384SHA384, 384) { }
		public ECDSAP384SHA384DNSSECKey(Dictionary<string, string> parameters) : base(SecObjectIdentifiers.SecP384r1, KeyAlgorithm.ECDSAP384SHA384, parameters) { }
	}
}
