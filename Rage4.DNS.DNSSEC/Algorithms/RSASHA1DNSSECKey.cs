﻿using System.Collections.Generic;

namespace Rage4.DNS.DNSSEC
{
	public class RSASHA1DNSSECKey : RSADNSSECKey
	{
		public RSASHA1DNSSECKey() : base(KeyAlgorithm.RSASHA1) { }
		public RSASHA1DNSSECKey(Dictionary<string, string> parameters) : base(KeyAlgorithm.RSASHA1, parameters) { }
	}
}
