﻿using System;
using System.Collections.Generic;
using System.Text;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Security;

namespace Rage4.DNS.DNSSEC
{
	public class ED448DNSSECKey : DNSSECKey
	{
		private Ed448PrivateKeyParameters privateParameters;
		private Ed448PublicKeyParameters publicParameters;

		public ED448DNSSECKey() : base(KeyAlgorithm.ED448)
		{
		}

		public ED448DNSSECKey(Dictionary<string, string> parameters) : base(KeyAlgorithm.ED448, parameters)
		{
			privateParameters = new Ed448PrivateKeyParameters(Convert.FromBase64String(parameters["PrivateKey"]), 0);
			publicParameters = privateParameters.GeneratePublicKey();
		}

		public override void CreateKey(KeyType keyType, int? bits)
		{
			var generator = new Ed448KeyPairGenerator();
			generator.Init(new Ed448KeyGenerationParameters(new SecureRandom()));

			var keyPair = generator.GenerateKeyPair();
			privateParameters = (Ed448PrivateKeyParameters)keyPair.Private;
			publicParameters = (Ed448PublicKeyParameters)keyPair.Public;
		}

		public override string GetPrivateKey()
		{
			return String.Format("Private-key-format: v1.2\nAlgorithm: {0} ({1})\nPrivateKey: {2}",
				(int)Algorithm, Enum.GetName(typeof(KeyAlgorithm), Algorithm),
				Convert.ToBase64String(privateParameters.GetEncoded())).Replace("_", "-");
		}

		public override byte[] GetPublicBytes()
		{
			return publicParameters.GetEncoded();
		}

		public override int GetBits() => 456;
    }
}
