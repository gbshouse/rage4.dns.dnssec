﻿using System.Collections.Generic;

namespace Rage4.DNS.DNSSEC
{
	public class RSASHA512DNSSECKey : RSADNSSECKey
	{
		public RSASHA512DNSSECKey() : base(KeyAlgorithm.RSASHA512) { }
		public RSASHA512DNSSECKey(Dictionary<string, string> parameters) : base(KeyAlgorithm.RSASHA512, parameters) { }
	}
}
