﻿using System;
using System.Collections.Generic;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;

namespace Rage4.DNS.DNSSEC
{
	public class RSADNSSECKey : DNSSECKey
	{
		private RsaPrivateCrtKeyParameters privateParameters;

		internal RSADNSSECKey(KeyAlgorithm algorithm) : base(algorithm) { }

		internal RSADNSSECKey(KeyAlgorithm algorithm, Dictionary<string, string> parameters) : base(algorithm, parameters)
		{
			privateParameters = new RsaPrivateCrtKeyParameters(
				new BigInteger(1, Convert.FromBase64String(parameters["Modulus"])),
				new BigInteger(1, Convert.FromBase64String(parameters["PublicExponent"])),
				new BigInteger(1, Convert.FromBase64String(parameters["PrivateExponent"])),
				new BigInteger(1, Convert.FromBase64String(parameters["Prime1"])),
				new BigInteger(1, Convert.FromBase64String(parameters["Prime2"])),
				new BigInteger(1, Convert.FromBase64String(parameters["Exponent1"])),
				new BigInteger(1, Convert.FromBase64String(parameters["Exponent2"])),
				new BigInteger(1, Convert.FromBase64String(parameters["Coefficient"]))
			);
		}

		public override void CreateKey(KeyType keyType, int? bits)
		{
			if (bits == null && keyType == KeyType.ZoneSigningKey)
				bits = 1024;
			else if (bits == null && keyType == KeyType.KeySigningKey)
				bits = 2048;
			else if ((bits % 1024) != 0 || bits < 1024 || bits > 4096)
				throw new ArgumentException("Incorrect bit size", "bits");

			RsaKeyPairGenerator generator = new RsaKeyPairGenerator();
			generator.Init(new KeyGenerationParameters(new SecureRandom(), bits ?? 2048));

			var keyPair = generator.GenerateKeyPair();
			privateParameters = (RsaPrivateCrtKeyParameters)keyPair.Private;
		}

		public override string GetPrivateKey()
		{
			return String.Format("Private-key-format: v1.2\nAlgorithm: {0} ({1})\nModulus: {2}\nPublicExponent: {3}\nPrivateExponent: {4}\nPrime1: {5}\nPrime2: {6}\nExponent1: {7}\nExponent2: {8}\nCoefficient: {9}",
				(int)Algorithm, Enum.GetName(typeof(KeyAlgorithm), Algorithm).Replace("_", "-"),
				Convert.ToBase64String(privateParameters.Modulus.ToByteArrayUnsigned()),
				Convert.ToBase64String(privateParameters.PublicExponent.ToByteArrayUnsigned()),
				Convert.ToBase64String(privateParameters.Exponent.ToByteArrayUnsigned()),
				Convert.ToBase64String(privateParameters.P.ToByteArrayUnsigned()),
				Convert.ToBase64String(privateParameters.Q.ToByteArrayUnsigned()),
				Convert.ToBase64String(privateParameters.DP.ToByteArrayUnsigned()),
				Convert.ToBase64String(privateParameters.DQ.ToByteArrayUnsigned()),
				Convert.ToBase64String(privateParameters.QInv.ToByteArrayUnsigned())).Replace("_", "-");
		}

		public override byte[] GetPublicBytes()
		{
			List<byte> buffer = new List<byte>
			{
				Convert.ToByte(privateParameters.PublicExponent.ToByteArrayUnsigned().Length)
			};
			buffer.AddRange(privateParameters.PublicExponent.ToByteArrayUnsigned());
			buffer.AddRange(privateParameters.Modulus.ToByteArrayUnsigned());
			return buffer.ToArray();
		}

        public override int GetBits()
        {
            return privateParameters.Modulus.BitLength;
        }
    }
}
