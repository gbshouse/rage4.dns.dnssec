﻿using System;
using System.Collections.Generic;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Nist;
using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;

namespace Rage4.DNS.DNSSEC
{
	public class ECDSADNSSECKey : DNSSECKey
	{
		private readonly DerObjectIdentifier oid;
		private readonly int bits;
		private ECPrivateKeyParameters privateParameters;
		private ECPublicKeyParameters publicParameters;

		private ECDomainParameters domainParameters => new ECDomainParameters(curveParameter.Curve, curveParameter.G, curveParameter.N, curveParameter.H, curveParameter.GetSeed());

		private X9ECParameters curveParameter => NistNamedCurves.GetByOid(oid);

		internal ECDSADNSSECKey(DerObjectIdentifier oid, KeyAlgorithm algorithm, int bits) : base(algorithm)
		{
			this.oid = oid;
			this.bits = bits;
		}

		internal ECDSADNSSECKey(DerObjectIdentifier oid, KeyAlgorithm algorithm, Dictionary<string, string> parameters) : base(algorithm, parameters)
		{
			this.oid = oid;

			privateParameters = new ECPrivateKeyParameters(new BigInteger(1, Convert.FromBase64String(parameters["PrivateKey"])), domainParameters);
			publicParameters = new ECPublicKeyParameters(curveParameter.G.Multiply(privateParameters.D), domainParameters);
		}

		public override byte[] GetPublicBytes()
		{
			var buffer = new List<byte>();
			buffer.AddRange(publicParameters.Q.XCoord.ToBigInteger().ToByteArrayUnsigned());
			buffer.AddRange(publicParameters.Q.YCoord.ToBigInteger().ToByteArrayUnsigned());
			return buffer.ToArray();
		}

		public override string GetPrivateKey()
		{
			return String.Format("Private-key-format: v1.2\nAlgorithm: {0} ({1})\nPrivateKey: {2}",
				(int)Algorithm, Enum.GetName(typeof(KeyAlgorithm), Algorithm),
				Convert.ToBase64String(privateParameters.D.ToByteArrayUnsigned())).Replace("_", "-");
		}

		public override void CreateKey(KeyType keyType, int? bits)
		{
			var generator = new ECKeyPairGenerator();
			generator.Init(new ECKeyGenerationParameters(domainParameters, new SecureRandom()));

			var keyPair = generator.GenerateKeyPair();
			privateParameters = (ECPrivateKeyParameters)keyPair.Private;
			publicParameters = (ECPublicKeyParameters)keyPair.Public;
        }

		public override int GetBits() => bits;
    }
}
