﻿using System.Collections.Generic;
using Org.BouncyCastle.Asn1.Sec;

namespace Rage4.DNS.DNSSEC
{
	public class ECDSAP256SHA256DNSSECKey : ECDSADNSSECKey
	{
		public ECDSAP256SHA256DNSSECKey() : base(SecObjectIdentifiers.SecP256r1, KeyAlgorithm.ECDSAP256SHA256, 256) { }
		public ECDSAP256SHA256DNSSECKey(Dictionary<string, string> parameters) : base(SecObjectIdentifiers.SecP256r1, KeyAlgorithm.ECDSAP256SHA256, parameters) { }
	}
}
