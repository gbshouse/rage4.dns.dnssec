﻿using System.Collections.Generic;

namespace Rage4.DNS.DNSSEC
{
	public class RSASHA256DNSSECKey : RSADNSSECKey
	{
		public RSASHA256DNSSECKey() : base(KeyAlgorithm.RSASHA256) { }
		public RSASHA256DNSSECKey(Dictionary<string, string> parameters) : base(KeyAlgorithm.RSASHA256, parameters) { }
	}
}
