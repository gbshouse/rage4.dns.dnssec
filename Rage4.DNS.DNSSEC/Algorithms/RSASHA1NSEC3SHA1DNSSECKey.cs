﻿using System.Collections.Generic;

namespace Rage4.DNS.DNSSEC
{
	public class RSASHA1NSEC3SHA1DNSSECKey : RSADNSSECKey
	{
		public RSASHA1NSEC3SHA1DNSSECKey() : base(KeyAlgorithm.RSASHA1_NSEC3_SHA1) { }
		public RSASHA1NSEC3SHA1DNSSECKey(Dictionary<string, string> parameters) : base(KeyAlgorithm.RSASHA1_NSEC3_SHA1, parameters) { }
	}
}
