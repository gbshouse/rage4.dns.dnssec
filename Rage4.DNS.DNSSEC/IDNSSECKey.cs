﻿namespace Rage4.DNS.DNSSEC
{
	public interface IDNSSECKey
	{
		KeyAlgorithm Algorithm { get; }

		void CreateKey(KeyType keyType, int? bits);

		string GetDNSKEY();

		string GetPrivateKey();

		int GetKeyTag(KeyType type);

		string GetDS(string domain, KeyType type, HashType hash);

		int GetBits();
	}
}
