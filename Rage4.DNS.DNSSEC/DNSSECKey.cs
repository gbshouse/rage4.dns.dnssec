﻿using System;
using System.Collections.Generic;

namespace Rage4.DNS.DNSSEC
{
	public abstract class DNSSECKey : IDNSSECKey
	{
		private readonly KeyAlgorithm algorithm;
        private readonly Dictionary<string, string> parameters;

        public KeyAlgorithm Algorithm => algorithm;

		internal Dictionary<string, string> Parameters => parameters;

		public DNSSECKey(KeyAlgorithm algorithm)
		{
			this.algorithm = algorithm;
		}

		public DNSSECKey(KeyAlgorithm algorithm, Dictionary<string, string> parameters)
		{
			this.algorithm = algorithm;
			this.parameters = parameters;
		}

		public string GetDNSKEY() => Convert.ToBase64String(GetPublicBytes());

		public int GetKeyTag(KeyType type) => DNSSECHelper.CreateKeyTag(type, GetDNSKEY(), algorithm);

		public string GetDS(string domain, KeyType type, HashType hash) => DNSSECHelper.CreateDS(domain, type, GetDNSKEY(), algorithm, hash);

		public abstract byte[] GetPublicBytes();

		public abstract string GetPrivateKey();

		public abstract void CreateKey(KeyType keyType, int? bits);

		public abstract int GetBits();
    }
}
