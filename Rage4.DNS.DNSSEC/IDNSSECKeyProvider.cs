﻿namespace Rage4.DNS.DNSSEC
{
	public interface IDNSSECKeyProvider
	{
		IDNSSECKey CreateDNSSECKey(KeyAlgorithm keyAlgorithm);
		IDNSSECKey LoadDNSSECKey(string input);
	}
}