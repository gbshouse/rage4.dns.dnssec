﻿using System;
using System.Linq;
using System.Text;

namespace Rage4.DNS.DNSSEC
{
	internal static class TypeHelpers
	{
		public static string ToHexString(this byte[] array)
		{
			return String.Join("", array.Select(o => o.ToString("x2")).ToArray());
		}

		public static byte[] HexStringToByteArray(this string value)
		{
			int len = value.Length;

			byte[] data = new byte[len / 2];
			for (int i = 0; i < len; i += 2)
			{
				data[i / 2] = (byte)((Convert.ToInt32(value[i].ToString(), 16) << 4) + Convert.ToInt32(value[i + 1].ToString(), 16));
			}
			return data;
		}

		public static string ToWireFormat(this string value)
		{
			if (String.IsNullOrEmpty(value))
				return String.Empty;

			int lenght;
			string output = String.Empty;

			foreach (string part in value.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries))
			{
				lenght = part.Length;
				output += lenght.ToString("x2");

				foreach (byte b in Encoding.ASCII.GetBytes(part))
					output += b.ToString("x2");
			}

			output += "00";

			return output;
		}

		public static string ToHex(this int value)
		{
			string output = value.ToString("x2");
			int length = output.Length % 2 == 0 ? output.Length : output.Length + 1;
			return output.PadLeft(length, '0');
		}

		public static string ToWireFormat(this string value, int flags, int protocol, int algorithm)
		{
			return value.ToWireFormat() + flags.ToHex() + protocol.ToHex() + algorithm.ToHex();
		}
	}

}
