﻿namespace Rage4.DNS.DNSSEC
{
	public enum HashType : int
	{
		SHA1 = 1,
		SHA256 = 2,
		SHA384 = 4
	}
}
