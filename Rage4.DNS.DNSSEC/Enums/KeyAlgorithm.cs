﻿namespace Rage4.DNS.DNSSEC
{
	public enum KeyAlgorithm : int
	{
		RSASHA1 = 5,
		RSASHA1_NSEC3_SHA1 = 7,
		RSASHA256 = 8,
		RSASHA512 = 10,
		ECDSAP256SHA256 = 13,
		ECDSAP384SHA384 = 14,
		ED25519 = 15,
		ED448 = 16
	}
}
