﻿namespace Rage4.DNS.DNSSEC
{
	public enum KeyType : int
	{
		ZoneSigningKey = 256,
		KeySigningKey = 257
	}
}
